import firebase from 'firebase'
import 'firebase/firestore'

// firebase init goes here
const config = {
    apiKey: "AIzaSyBkT1KuJGJq0l9LM8izdFYP3l6KbXdz9T8",
    authDomain: "simple-blog-f1695.firebaseapp.com",
    databaseURL: "https://simple-blog-f1695.firebaseio.com",
    projectId: "simple-blog-f1695",
    storageBucket: "simple-blog-f1695.appspot.com",
    messagingSenderId: "481758771557"
}
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

// date issue fix according to firebase
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings)

// firebase collections
const usersCollection = db.collection('users')
const postsCollection = db.collection("posts")
const commentsCollection = db.collection("comments")
const likesCollection = db.collection('likes')

export {
    firebase,
    db,
    auth,
    currentUser,
    usersCollection,
    postsCollection,
    commentsCollection,
    likesCollection
}
