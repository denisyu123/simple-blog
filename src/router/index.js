import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'

import { store } from '@/store.js'

import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'
import Settings from '@/components/Settings'
import Blog from '@/components/Blog'
import Post from '@/components/Post'
import About from '@/components/About'


Vue.use(Router)

const router = new Router({
	mode: 'history',
	routes: [
		{
			path: '*',
			redirect: '/'
		},
		{
			path: '/',
			name: 'About',
			component: About,
			meta: {
				requiresAuth: false,
				title: 'Dennis Yu Personal Website',
				root: true
            }
		},
		{
			path: '/post/:postId',
			name: 'Post',
			component: Post,
			meta: { title: 'Loading' }
		},
		{
			path: '/blog',
			name: 'Blog',
			component: Blog,
			meta: { title: 'Blog' }
		},
		{
			path: '/login',
			name: 'Login',
			component: Login,
			meta: { title: 'Login' }
		},
        {
			path: '/dashboard',
			name: 'Dashboard',
			component: Dashboard,
            meta: {
				requiresAuth: true,
				title: 'Dashboard'
            }
		},
        {
			path: '/settings',
			name: 'Settings',
			component: Settings,
            meta: {
				requiresAuth: true,
				title: 'Settings'
            }
		}
	],
	scrollBehavior (to, from, savedPosition) {
	// return desired position
		if (savedPosition) {
			return savedPosition
		} 
		else {
			return { x: 0, y: 0 }
		}
	}
})

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
	const currentUser = firebase.auth().currentUser
	const root = to.matched.some(x => x.meta.root)
	document.title = to.meta.title

	//update header
	if(root){
		store.commit('setHeader', {
            title: '',
            displayLogo: true
        })
	}
	else{
		store.commit('setHeader', {
            title: to.meta.title,
            displayLogo: false
        })
	}

    if (requiresAuth && !currentUser) {
        next('/login')
    } else if (requiresAuth && currentUser) {
        next()
    } else {
		// logon user does not need login page ;)
		if(currentUser && to.path == "/login"){
			next('/')
		}
		else{
			next()
		}
    }
})

export default router
