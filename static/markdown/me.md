<p align="center">
    <img class="circle" width="180px" src="https://pbs.twimg.com/profile_images/1147083356623921154/QpHViE_w_bigger.jpg">
    <h2 align="center">Full-stack Developer</h2>
</p>
<p align="center">
<a href="https://www.linkedin.com/in/dennisyu0203/"><img src="https://img.shields.io/badge/Linkedin-blue.svg?logo=linkedin&logoColor=white" alt="Linkedin"></a>
<a href="https://t.me/kinho"><img src="https://img.shields.io/badge/Telegram-blue.svg?logo=telegram&logoColor=white" alt="Telegram"></a>
<a href="https://twitter.com/dennisyu123"><img src="https://img.shields.io/twitter/follow/dennisyu123.svg?style=social&label=Follow" alt="Follow on Twitter"></a>
</p>



### Skills

* Java ( Spark / Spring / Hibernate / Hybris / Android / JSP? )
* Web frontend ( jquery / Vue / PHP? )
* Swift 4.2 ( IOS )
* SQL ( MySQL / SQL Server ) Realtime DB ( Firebase )
* HTML / XHTML / XML
* BI or Report Tools ( Qlikview / Report Builder SSRS )

### Hobbies

* Games (FPS, ARPG, RPG etc...)
* Coding (That why this site exists)
* Japanese anime